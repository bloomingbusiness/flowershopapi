# Installation et configuration

## Initialisation du projet
`npm init`

##### Installation de yarn
`npm install --global yarn`

## Installation des packages nécéssaires

##### Utilitaire qui permet de redémarrer automatiquement le serveur lorsqu'un fichier est modifié
`yarn add nodemon`

##### Installation framework nodejs avec express
`yarn add express` 

##### Installation du package pour la BDD
`yarn add pg` 

##### Installation de cors qui permet de ne plus rendre bloquant l'accès aux ressources via les requêtes
`yarn add cors`

## Lancement de l'api
`yarn start`
