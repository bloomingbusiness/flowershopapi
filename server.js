const express = require('express')
const cors = require('cors')
const dotenv = require('dotenv')
const healthCheckRoutes = require('./src/routes/healthcheck')

dotenv.config()

const app = express()
const port = dotenv.SERVICE_PORT || 3000;

app.use(express.json())
app.use(cors())

app.use('/healthcheck', healthCheckRoutes);

app.get("/", (req, res) => {
    res.send("Hello world")
})

app.listen(port, () => console.log(`Server is listening on port ${port}`))
